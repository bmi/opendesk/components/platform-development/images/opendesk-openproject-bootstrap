#!/bin/bash
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

set -e

if [[ $OD_OP_BOOTSTRAP_DEBUG_ENABLED == "true"  ]] ; then
  echo "Debug mode is enabled"
  set -x
  set -v
fi

export INTEGRATION_SETUP_DEBUG=${OD_OP_BOOTSTRAP_DEBUG_ENABLED}
export INTEGRATION_SETUP_TEMP_DIR=${OD_OP_BOOTSTRAP_TEMP_DIR}

regex_check_int='^[0-9]+$'
if [[ $OD_OP_BOOTSTRAP_DEBUG_PAUSE_BEFORE_SCRIPT_START =~ $regex_check_int ]] ; then
  echo "Pausing $OD_OP_BOOTSTRAP_DEBUG_PAUSE_BEFORE_SCRIPT_START seconds before starting the script."
  sleep $OD_OP_BOOTSTRAP_DEBUG_PAUSE_BEFORE_SCRIPT_START
fi

# run the upstream script
./integration_setup.sh
