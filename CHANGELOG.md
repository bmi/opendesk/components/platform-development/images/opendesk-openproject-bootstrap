## [1.1.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-openproject-bootstrap/compare/v1.1.3...v1.1.4) (2024-08-02)


### Bug Fixes

* **integration_setup.sh:** Use update script from GitHub; Resolve dockerlink errors/warnings. ([c82e38a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-openproject-bootstrap/commit/c82e38a9f3ea3db7b387dde5f36c40cbdcbe0e6f))

## [1.1.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-openproject-bootstrap/compare/v1.1.2...v1.1.3) (2023-12-27)


### Bug Fixes

* **ci:** Move to Open CoDE ([2ea3f5e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-openproject-bootstrap/commit/2ea3f5e21d8d3a28912bc174cc9e71b49ea5c9a9))

## [1.1.2](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/compare/v1.1.1...v1.1.2) (2023-12-07)


### Bug Fixes

* **docker:** Remove apk cache ([a6d0257](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/a6d0257be9ef50f468630353b9bff4eabd46d1a4))

## [1.1.1](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/compare/v1.1.0...v1.1.1) (2023-11-12)


### Bug Fixes

* **entrypoint:** Shebang as first line in entrypoint.sh ([4ad6cee](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/4ad6cee44c9bfaae7e8cfd1d3e672d2021452086))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/compare/v1.0.3...v1.1.0) (2023-11-12)


### Features

* **Dockerfile:** Use alpine as base image to reduce size. Fix license information ([d0c4b1f](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/d0c4b1f0472075896ce580b5bf9b4b740e9dbd61))

## [1.0.3](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/compare/v1.0.2...v1.0.3) (2023-11-11)


### Bug Fixes

* **ci:** Add EOF linebreak for yamllint ([024679b](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/024679b83fa57808b7ce152f08c10fb79868a657))

## [1.0.2](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/compare/v1.0.1...v1.0.2) (2023-11-11)


### Bug Fixes

* **scripts:** Improve error handling ([b9edcb8](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/b9edcb89a9f7ce3984399703895b36407904ffc7))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/compare/v1.0.0...v1.0.1) (2023-11-11)


### Bug Fixes

* **scripts:** Improve script tempdir handling ([aff6429](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/aff6429617503fd8d4a6cb832e5eb718e88ebcfe))

# 1.0.0 (2023-11-11)


### Bug Fixes

* **ci:** Start versioning ([ffefe96](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/ffefe96693967ac4da73e7de54dd486a79c75aed))
* **docker:** Add required modules for script ([31dd661](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/31dd6618c643a074954911f61291f479d0dccdb3))
* **dockerfile:** Add user and start semver releasing ([5a1135b](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/5a1135b2bcfee9d8960a3f4f9b5991cb404769c7))
* **entrypoint.sh:** Add sleep for further debugging ([2b75581](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/2b755810119fc2138e7d3c4844d18bb056c71864))
* **entrypoint:** Fix debug pause ([88346f5](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/88346f56d6c6c89bc5fa9c5a6242ba317604991a))
* **entrypoint:** Fix name of env variable ([73fccbd](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/73fccbd234c764a99f52770a509ef491616872c5))
* Initial Commit ([701499a](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/701499a6feb77afee1980a78ee12df339f729215))
* **integration:** better support for debug output ([ca98756](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/ca98756f48fc60bf1ca2d852122fb628cf0fd629))
* **integration:** Fix jq call ([456159e](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/456159edff0c8efa2b8a2b27cad35b40705d4bbb))
* **integrationscript:** externalize requests ([edaeea9](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/edaeea9d9dbe6f6971fe76b3077a3b8d485bbfd0))
* **script:** Extend debugging options ([f615988](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/f6159887db4d3c7048e46fff2df76270586396bb))
* **scripts:** Add support for temporary directory ([d9726e5](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/d9726e5e57bdd21f2b3b060fd5912eca3709f443))
* **scripts:** Add support for temporary directory ([b2fd922](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/b2fd922722b4852187544e3ef22cdc153bfe8d2e))
* **script:** Update integration script ([79a07cb](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/79a07cbcbbb27546065bf9d7f3b54cd5a6458481))
* **script:** Use local script ([6a4987c](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/6a4987c5370a0eac5f4040096725e569cc8bda09))
* **ssl:** Add ca-certificates to fetch with wget ([eb0b339](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/eb0b3399daa8624a96cf4132250fbf7a8bed5e24))
* **ssl:** Add ca-certificates to fetch with wget ([77ea3fd](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/77ea3fd2d78d90044675093148435f47cc58b04c))
* Temporary disable cert check on wget ([a933ac8](https://gitlab.souvap-univention.de/souvap/tooling/images/opendesk-openproject-bootstrap/commit/a933ac867ee287ce180b183f6f5b5ee1e71e7393))

<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
