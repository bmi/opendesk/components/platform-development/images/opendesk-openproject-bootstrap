<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# openDesk OpenProject Bootstrap Image

This image is used to bootstrap OpenProject within openDesk by
- setting up the integration between OpenProject and Nextcloud.

# License

This project uses the following license: AGPL-3.0-only

# Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"