# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

FROM docker.io/alpine:3.20.2@sha256:0a4eaa0eecf5f8c050e5bba433f58c052be7587ee8af3e8b3910ef9ab5fbe9f5

WORKDIR /app

RUN apk add --no-cache \
      jq=1.7.1-r0 \
      curl=8.9.0-r0 \
      bash=5.2.26-r0 \
 && addgroup -S "app" \
 && adduser -D -G "app" -h "/app" -s "/bin/bash" -u 1000 -S "app" \
 && curl https://raw.githubusercontent.com/rossnet/integration_openproject/91e8bbcd4d2cfa6f4b9b0eb175c5881ee4a99518/integration_setup.sh >/app/integration_setup.sh \
 && chmod 777 /app/integration_setup.sh

USER app

COPY ./bootstrap-openproject/entrypoint.sh /app

CMD ["/bin/bash", "-c", "/app/entrypoint.sh"]
